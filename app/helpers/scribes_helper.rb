module ScribesHelper

  def timeToSecs(time)
    timeSplit = time.to_s.split(":")
    hours = timeSplit[0].to_i  * 3600
    minutes = timeSplit[1].to_i  * 60
    seconds = timeSplit[2].to_i 
    return hours + minutes + seconds
  end

  def secToTime(seconds)
    hours = (seconds / 3600).floor
    minutes = ((seconds - (hours * 3600)) / 60).floor
    secs = seconds - (hours * 3600) - (minutes * 60)
    
    hours = hours.to_s
    minutes = minutes.to_s
    secs = secs.to_s

    if minutes.length < 2
      minutes = "0" + minutes
    end

    if secs.length < 2
      secs = "0" + secs
    end

    return  hours + ":" + minutes + ":" + secs
  end

  def playerInject(video_id)
    "<script>var tag=document.createElement('script');tag.src=\"https://www.youtube.com/iframe_api\";var firstScriptTag=document.getElementsByTagName('script')[0];firstScriptTag.parentNode.insertBefore(tag,firstScriptTag);var player;function onYouTubeIframeAPIReady(){player=new YT.Player('player',{height: '450',videoId:'" + video_id + "',events:{'onReady':onPlayerReady}})}function onPlayerReady(event){event.target.playVideo();}</script>"
  end

  def scribeImage(video_id)
    "https://i.ytimg.com/vi/" + video_id + "/hqdefault.jpg"
  end

end

