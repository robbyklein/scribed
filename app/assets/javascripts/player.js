//////////////////////////////////////////////////////////////
// Video ID Extraction
/////////////////////////////////////////////////////////////
function extractVideoId(input) {
  if (input.length == 11) {
    return input
  } 
  else {
    var exists = input.indexOf("watch?v=")
    if (exists > -1) {
      var index = exists + 8
      return input.substr(index, 11)
    } else {
      return false
    }
  }
}

//////////////////////////////////////////////////////////////
// Api Data Call Stuff
/////////////////////////////////////////////////////////////
function apiUrl(videoId) {
  baseUrl = 'https://www.googleapis.com/youtube/v3/videos?id='
  apiKey = '&key=' + 'AIzaSyCR3ttiCz8DlocdHYK-RfZCVCnIp651-74'
  parts = '&part=snippet,contentDetails,status'
  fullUrl = baseUrl + videoId + apiKey + parts
  return fullUrl
}

function getVideo (videoId) {
  fullUrl = apiUrl(videoId)
  // Retrieve the Json
  $.getJSON(fullUrl, callbackFuncWithData);

  // Function to run when recieved
  function callbackFuncWithData(data) {

    if( data.items.length == 0 ) {
      $('.loader').removeClass('active')
      $('.create-scribe').append('<p class="retrieve-error rounded">Invalid Video ID or URL</p>')
    } 
    else if (data.items[0].status.embeddable == false) {
      $('.loader').removeClass('active')
      $('.create-scribe').append('<p class="retrieve-error rounded">Sorry this video is not embeddable</p>')
    } 
    else {
      $('.youtube-url, .retrieve').prop("disabled", true);
      var title = data.items[0].snippet.title
      ytDescription = data.items[0].snippet.description
      var id = extractVideoId($('.youtube-url').val())
      videoLength = ptToSec(data.items[0].contentDetails.duration)

      $('#scribe_video_length').val(videoLength)
      $('#scribe_title').val(title)
      $('#scribe_video_id').val(id)
      ytInject(id)
    }

  }
}

//////////////////////////////////////////////////////////////
// Time Converters
/////////////////////////////////////////////////////////////
function ptToSec(duration) {
    var matches = duration.match(/[0-9]+[HMS]/g);
    var seconds = 0;
    matches.forEach(function (part) {
        var unit = part.charAt(part.length-1);
        var amount = parseInt(part.slice(0,-1));
        switch (unit) {
            case 'H':
                seconds += amount*60*60;
                break;
            case 'M':
                seconds += amount*60;
                break;
            case 'S':
                seconds += amount;
                break;
            default:
                // noop
        }
    });
    return seconds;
}

function secToTime (seconds) {
  var hours = Math.floor(seconds / 3600)
  var minutes = addZero(Math.floor((seconds - (hours * 3600)) / 60))
  var secs = addZero(seconds - (hours * 3600) - (minutes * 60))
  return  hours + ":" + minutes + ":" + secs
}

function timeToSec (time) {
  var time = time.split(':')
  return parseInt(time[0] * 3600) + parseInt(time[1] * 60) + parseInt(time[2])  
}

function addZero(num) {
  (String(num).length < 2) ? num = String("0" + num) :  num = String(num);
  return num;   
}

//////////////////////////////////////////////////////////////
// Player Injector
/////////////////////////////////////////////////////////////
function ytInject(videoid) {
  var script = "<script>var tag=document.createElement('script');tag.src=\"https://www.youtube.com/iframe_api\";var firstScriptTag=document.getElementsByTagName('script')[0];firstScriptTag.parentNode.insertBefore(tag,firstScriptTag);var player;function onYouTubeIframeAPIReady(){player=new YT.Player('player',{height:'206',width:'338',videoId:'" + videoid + "',events:{'onReady':onPlayerReady}})}</script>"
  $('body').append(script)
}

//////////////////////////////////////////////////////////////
// Player Functions
/////////////////////////////////////////////////////////////
function jumpTo(time) {
  player.seekTo(time, true);
}

function onPlayerReady(event) {
  $('.loader').removeClass('active')
  sectionFadeIn()
  event.target.playVideo();
}

//////////////////////////////////////////////////////////////
// Submit Delay
/////////////////////////////////////////////////////////////
function fsubmit() {   
  $("form").submit();
}

function nameAdder(array) {
  var count = 0
  array.each(function(){
    var value = "script[" + count + "]"
    $(this).attr('name', value)
    count += 1
  })
}


//////////////////////////////////////////////////////////////
// Validations
/////////////////////////////////////////////////////////////
function emptyCheck(input) {
    valid = false
}

function numberCheck(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

function timeFormatCheck(time) {
  var timeSplit = time.split(':')
  var seconds = timeToSec(time)


  // Checks for three parts
  if (timeSplit.length !== 3 || isNaN(seconds)) {
    valid = false
    return 1
  }

  // Converts to integers
  for(var i=0; i<timeSplit.length; i++) { 
    timeSplit[i] = +timeSplit[i]; 
  }

  if (seconds > videoLength || seconds < 0) {
    valid = false
    return 2
  } else {
    console.log(seconds)
  }

}

function sectionFadeIn() {
  $('.create-player-column, .create-scribes-column, .create-scribe-submit').addClass('active')
}