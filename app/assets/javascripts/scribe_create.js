$(document).ready(function(){

  // Count of Scribes
  scribe_count = $('.transcript').length
  ytDescription = "";

  $('.use-yt-desc').click(function(){
    $('#scribe_description').val(ytDescription)
    $('.scribe-description-length p').text( $('#scribe_description').val().length )
  })

  // Checks for video id on page load
  $('.youtube-url').val( $('#scribe_video_id').val() )

  if ( $('.youtube-url').val().length > 0 ) {
    var valid = extractVideoId( $('.youtube-url').val() )
    if (valid !== false) {
      $('.invalid').removeClass('active')
      $('.retrieve').prop("disabled", false);
      $('.retrieve').addClass('active')
    } else {
      $('.retrieve').removeClass('active')
      $('.retrieve').prop("disabled", true);
      $('.invalid').addClass('active')
    }
  }

  // Enables Submit if more than two scribes on load
  if(scribe_count < 2) {
    $('#create-scribe-submit').val("Add Atleast two scribes");
    $('#create-scribe-submit').prop("disabled", true);
  }

  // Checks Scribe description onload
  $('.scribe-description-length p').text( $('#scribe_description').val().length )

  // Scribe Description on load
  // Checks input on keyup
  $('#scribe_description').keyup(function(){
    var length = $(this).val().length
    $('.scribe-description-length p').text(length)

  })



  $('body').on('click', '.add-scribe', function(event) {
    event.preventDefault()
    
    var time = player.getCurrentTime()

    $('.scribe-list').prepend('<div class="transcript"><input class="s transcript-description" placeholder="What\'s happening?" type="text"/><div class="transcript-time"> <div class="transcript-time-controls"> <input class="s time" value="' + secToTime(Math.floor(time)) + '" type="text" disabled/> <button class="time-up">+</button> <button class="time-down">+</button> </div></div><button class="remove-scribe">X</button> <button class="to-scribe">GO</button><button class="scribe-up">UP</button><button class="scribe-down">do</button></div>')

    scribe_count += 1

    if (scribe_count > 1) {
      $('#create-scribe-submit, #edit-scribe-submit').val('submit')
      $('#create-scribe-submit').prop("disabled", false);
      $('#create-scribe-submit').addClass('active');
    }

    $('.transcript-description').eq(0).focus()
  })

  // Checks input on keyup
  $('.youtube-url').keyup(function(){
    if ( $(this).val().length > 0 ) {
      var valid = extractVideoId( $(this).val() )
      if (valid !== false) {
        $('.invalid').removeClass('active')
        $('.retrieve').prop("disabled", false);
        $('.retrieve').addClass('active')

      } else {
        $('.retrieve').removeClass('active')
        $('.retrieve').prop("disabled", true);
        $('.invalid').addClass('active')
      }
    } else {
      $('.invalid, .retrieve').removeClass('active')
    }
  })

  // Makes Ajax Call
  $('.retrieve').click(function(event){
    event.preventDefault()
      $('.loader').addClass('active')

    $('.retrieve-error').remove()
    $('.details-column').empty()

    var videoid = extractVideoId( $('.youtube-url').val() )
    getVideo(videoid)
  })

  $('body').on('click', '.scribe-up', function(event) {
    event.preventDefault()
    var index = $(this).parent().index('.transcript')
    if (index > 0) {
      var element = $(this).parent()
      $(this).parent().remove()
      element.insertBefore($('.transcript').eq(index-1))
    }
  })

  $('body').on('click', '.scribe-down', function(event) {
    event.preventDefault()
    var count = $('.transcript').length
    var index = $(this).parent().index('.transcript')

    if (index != (count-1)) {
      var element = $(this).parent()
      $(this).parent().remove()
      element.insertAfter($('.transcript').eq(index))
    }
  })



  $('body').on('click', '.to-scribe', function(event) {
    event.preventDefault()
    var time = timeToSec($(this).siblings('.transcript-time').children().children('.time').val())
    console.log(time)
    player.seekTo(time)
  })

  $('body').on('click', '.back-15', function(event) {
      event.preventDefault()
    player.seekTo(player.getCurrentTime() - 15)
  })

  $('body').on('click', '.remove-scribe', function(event) {
      event.preventDefault()
      $(this).parent().remove()
  })

  $('#create-scribe-submit, #edit-scribe-submit').click( function(ev) {
    ev.preventDefault(); 
    nameAdder($('.s'))
    $('.time').prop("disabled", false);
    fsubmit($(this))
  });

$('body').on('click', '.time-up', function(event) {
  event.preventDefault()
  currentTime = timeToSec($(this).parent().children('.time').val())
  newTime = secToTime(currentTime + 1)
  $(this).parent().children('.time').val(newTime)
})

$('body').on('click', '.time-down', function(event) {
  event.preventDefault()
  currentTime = timeToSec($(this).parent().children('.time').val())
  newTime = secToTime(currentTime - 1)
  if ( currentTime > 0 ) {
    $(this).parent().children('.time').val(newTime)
  }
})


$('#new_scribe, .edit_scribe').submit(function (event) {
  var valid = true
  //Errors
  var errors = []

  // Possible Errors
  var scribe_to_short = false //
  var scribe_to_long = false //

  var time_format = false //

  var title_to_short = false //
  var title_to_long = false //

  var description_to_short = false //
  var description_to_long = false //

  // Removes extra whitespace
  var title_squish = $('#scribe_title').val().replace(/\s{2,}/g,' ');
  $('#scribe_title').val(title_squish)

  var description_squish = $('#scribe_description').val().replace(/\s{2,}/g,' ');
  $('#scribe_description').val(description_squish)

  // Checks title length
  if ( $('#scribe_title').val().length > 100 ) {
    var title_to_long = true
    valid = false
  } else if ( $('#scribe_title').val().length < 4 ) {
    var title_to_short = true
    valid = false
  }

  //checks description length
  if ( $('#scribe_description').val().length > 2500 ) {
    var description_to_long = true
    valid = false
  } else if ( $('#scribe_description').val().length < 4 ) {
    var description_to_short = true
    valid = false
  }

  // Empty Desc Check
  $('.transcript-description').each(function(){

    var squished = $(this).val().replace(/\s{2,}/g,' ');
    $(this).val(squished)

    if( $(this).val().length < 4 ) {
      scribe_to_short = true
      valid = false
    } else if ( $(this).val().length > 100 ) {
      scribe_to_long = true
      valid = false    
    }
  })

  // Time Calidation
  $('.time').each(function(){
    if( $(this).val().length !== 7 ) {
      time_format = true
      valid = false
    }

    var seconds = timeToSec($(this).val())

    if (isNaN(seconds)) {
      time_format = true
      valid = false   
    } else if ( seconds > $('#scribe_video_length').val() || seconds < 0 ) {
      time_format = true
      valid = false         
    }

  })

  if (title_to_short == true) {
    errors.push('Video title too short (4 character minimum)')
  }

  if (title_to_long == true) {
    errors.push('Video title too long (100 character maximim)')
  }

  if (description_to_short == true) {
    errors.push('Video description too short (4 character minimum)')
  }

  if (description_to_long == true) {
    errors.push('Video description too long (2500 character maximum)')
  }

  if (scribe_to_short == true) {
    errors.push('Scribe description too short (4 character minimum)')
  }

  if (scribe_to_long == true) {
    errors.push('Scribe description too long (100 character maximum)')
  }

  if (time_format == true) {
    errors.push('Time format error')
  }

  console.log(errors)

  if (valid == false) {
    return false
  }

})


})





