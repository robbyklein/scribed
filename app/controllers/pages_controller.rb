class PagesController < ApplicationController
  def home
  end

  def admin
    if user_signed_in? && current_user.admin?
      @scribes = Scribe.all.order("created_at DESC")
      @users = User.all.order("created_at DESC")
      #@comments = Comment.all.order("created_at DESC")
    else
      redirect_to "/"
      flash[:error] = "You don't belong there."
    end
  end
end
