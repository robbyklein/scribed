class UsersController < ApplicationController
  def show
    @user = User.find_by!(permalink: params[:permalink])
  end

  def logs
    @user = User.find_by!(permalink: params[:permalink])
    @scribes = @user.scribes.all.order("created_at DESC").page(params[:page]).per(6)  end
end
