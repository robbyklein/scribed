    class ScribesController < ApplicationController

      def new
          @scribe = Scribe.new
      end

      def create
        @scribe = Scribe.new(scribe_params)

        if user_signed_in?
          @scribe.user_id = current_user.id
        else
          @scribe.user_id = "2"
        end

        @scribe.time_script = params[:script]
        
        if @scribe.save
          redirect_to '/'
        else
          render :edit
          flash[:error] = "You don't belong there."
        end
      end

      def edit
        if user_signed_in?
          @scribe = Scribe.find_by!(id: params[:id])
        else
          redirect_to "/"
          flash[:error] = "You don't belong there."
        end
      end

      def update

        @scribe = Scribe.find_by!(id: params[:id])

        if user_signed_in?
          @scribe.user_id = current_user.id
        else
          @scribe.user_id = "2"
        end

        @scribe.time_script = params[:script]
        
        if @scribe.save
          redirect_to '/'
        else
          render :edit
          flash[:error] = "You don't belong there."
        end
      end

      def index
        if params[:search]
          @scribes = Scribe.search(params[:search]).order("created_at DESC").page(params[:page]).per(9)
        else
          @scribes = Scribe.all.order("created_at DESC").page(params[:page]).per(6)
        end
      end

      def show
        @scribe = Scribe.find_by!(id: params[:id])
        if @scribe.user_id.present?
          @user = User.find(@scribe.user_id)
        end
        @random = Scribe.order("RAND()").last(3)
        @comment = Comment.new
        @comment.scribe_id = Scribe.find_by!(id: params[:id]).id
        @comments = @scribe.comments.order("created_at DESC")
      end

      def destroy
        if user_signed_in? && current_user.id == Scribe.find_by!(id: params[:id]).user_id
          @scribe = Scribe.find_by!(id: params[:id])
          @scribe.destroy
          redirect_to admin_path
        else
          redirect_to "/create"
          flash[:error] = "Please Don't :("
        end
      end

      def vote
        value = params[:type] == "up" ? 1 : -1
        @scribe = Scribe.find(params[:id])
        @scribe.add_or_update_evaluation(:votes, value, current_user)
        redirect_to :back, notice: "Thank you for voting!"
      end

      private
        def scribe_params
          params.require(:scribe).permit(:title, :description, :script, :permalink,  :video_length, :user_id, :transcripts, :video_id)
        end
    end
