class CommentsController < ApplicationController
  def create
    @comment = Comment.new(comment_params)
    @comment.user_id = current_user.id
    if @comment.save
      redirect_to '/'
    end
  end


  private
    def comment_params
      params.require(:comment).permit(:comment, :user_id, :scribe_id)
    end
end
