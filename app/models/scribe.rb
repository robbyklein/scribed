require 'open-uri'
require 'json'
require 'time'
require 'ruby-duration'
require 'resolv-replace'

class YoutubeIdValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    good = true

    url = "https://www.googleapis.com/youtube/v3/videos?id=" + record.video_id + "&key=AIzaSyCR3ttiCz8DlocdHYK-RfZCVCnIp651-74&part=snippet,contentDetails"
    file  = open(url) {|f| f.read }
    json = JSON.parse file
    len = ISO8601::Duration.new(json["items"][0]["contentDetails"]["duration"]).to_seconds

    unless len == value.to_i
      good = false
    end

    unless good
      record.errors[:scribe] << "Something doesn't add upp"
    end
  end
end

class TwoValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    good = true

    unless value.length > 1 && value.length < 100
      good = false
    end

    unless good
      record.errors[:scribe] << "Need between 2-100 scribes"
    end
  end
end

class ScriptIdValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    good = true

    url = "https://www.googleapis.com/youtube/v3/videos?id=" + value + "&key=AIzaSyCR3ttiCz8DlocdHYK-RfZCVCnIp651-74&part=snippet"
    file  = open(url) {|f| f.read }
    json = JSON.parse file
    items = json["items"]
      unless items.length == 1
        good = false
      end

    unless good
      record.errors[:scribe] << 'invalid youtube id'
    end
  end
end

class ScriptPresenceValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    good = true

    value.each do |v|
      a = v[0]
      b = v[1]

      unless a.to_s.length > 0 && b.to_s.length > 0
        good = false
      end
    end

    unless good
      record.errors[:scribe] << 'descriptions cannot be blank'
    end
  end
end

class ScriptLengthValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    good = true

    value.each do |t|
      unless t[0] < record.video_length.to_i
        good = false
      end
    end

    unless good
      record.errors[:scribe] << 'time exceeds video length'
    end
  end
end


class Scribe < ActiveRecord::Base
  belongs_to :user

  has_reputation :votes, source: :user, aggregated_by: :sum

  serialize :transcripts, Array

  has_many :comments

  validates :title, presence: true
  validates :description, presence: true
  validates :video_id, :script_id => true
  validates :transcripts, :two => true

  validates :transcripts, :script_length => true

  validates :transcripts, :script_presence => true

  validates :video_length, :youtube_id => true
  validates :title, length: { minimum: 4, maximum: 100 }
  validates :description, length: { minimum: 4, maximum: 2500 }

  before_save :youtube_info, :download_image, :squish_stuff

def self.search(query)
# where(:title, query) -> This would return an exact match of the query
where("title like ?", "%#{query}%") 
end

  def time_script=(script)
    scripts = {}


    def timeToSecs(time)
      timeSplit = time.to_s.split(":")
      hours = timeSplit[0].to_i  * 3600
      minutes = timeSplit[1].to_i  * 60
      seconds = timeSplit[2].to_i 
      return hours + minutes + seconds
    end

    unless script == nil
      a = 0
      b = 1

      while b < script.length
        if script["#{b}"].length == 0
          script["#{b}"] = 99999
        else
          script["#{b}"] = timeToSecs(script["#{b}"])
        end

        b += 2
      end

      while a < script.length
        scripts[script["#{a + 1}"]] = script["#{a}"]
        a += 2
      end


      self.transcripts = scripts.sort
    end
  end

  def apiUrl(youtube_id)
    base_url = "https://www.googleapis.com/youtube/v3/videos?id="
    key = "&key=AIzaSyCR3ttiCz8DlocdHYK-RfZCVCnIp651-74"
    part = "&part=snippet,contentDetails,status"
    return base_url + youtube_id + key + part
  end

  def squish_stuff
    self.title = self.title.squish
    self.description = self.description.squish
  end

  def youtube_info
    url = apiUrl(self.video_id)
    file  = open(url) {|f| f.read }
    json = JSON.parse file
    self.video_length = ISO8601::Duration.new(json["items"][0]["contentDetails"]["duration"]).to_seconds
    self.video_image = json["items"][0]["snippet"]["thumbnails"]["high"]["url"]
  end

  def download_image
    filename = "public/scribe_images/" + self.video_id + ".jpg"
    open(filename, 'wb') do |file|
      file << open(self.video_image).read
    end
    ggg = Magick::CenterGravity

    source = Magick::Image.read(filename).first
    source = source.crop!(ggg,480,268)
    source.write(filename)

    self.video_image = "/scribe_images/" + self.video_id + ".jpg"
  end


end
