class User < ActiveRecord::Base
  devise :database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :trackable, :validatable

  validates :username, presence: true
  validates :username, length: {
    minimum: 2,
    maximum: 30,
    too_short: "must have at least %{count} words",
    too_long: "must have at most %{count} words"
  }
  validates :username, format: { with: /^[0-9a-zA-Z]*$/, :multiline => true,
    message: "fewf" }

  has_many :scribes
  has_many :comments

  before_save :set_permalink

  has_reputation :votes, source: {reputation: :votes, of: :scribes}, aggregated_by: :sum

  def to_param
    self.permalink
  end

  def url
    "/users/#{self.permalink}"
  end

  private
  def set_permalink
    self.permalink = self.username.downcase
  end

end