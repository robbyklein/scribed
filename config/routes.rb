    Rails.application.routes.draw do
      get 'pages/home'

      devise_for :users, path_names: {sign_in: 'login', sign_out: 'logout'}
      root 'pages#home'
      resources :comments
      resources :scribes do
        member { post :vote }
      end
      match 'users/:permalink' => 'users#show', via: :get, as: :user_show
      match 'users/:permalink/logs' => 'users#logs', via: :get, as: :user_logs

      match 'create' => 'scribes#new', via: :get
      match 'admin' => 'pages#admin', via: :get

      # Post Show
      get 'tags/:tag', to: 'scribes#index', as: :tag
    end
