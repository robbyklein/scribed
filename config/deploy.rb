lock '3.4.0'

set :application, 'ytlogs'
set :ssh_options, {:forward_agent => true}
set :repo_url, 'git@bitbucket.org:robbyklein/scribed.git'

set :deploy_to, '/var/www/ytlogs.com'

set :linked_files, %w{config/database.yml config/secrets.yml}
set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system public/scribe_images}

namespace :deploy do

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      execute :touch, release_path.join('tmp/restart.txt')
    end
  end

  after :publishing, 'deploy:restart'
  after :finishing, 'deploy:cleanup'
end

