class CreateScribes < ActiveRecord::Migration
  def change
    create_table :scribes do |t|
      t.string :title
      t.string :video_id
      t.string :video_length
      t.string :video_image
      t.text :description
      t.string :permalink
      t.text :transcripts

      t.references :user, index: true

      t.timestamps null: false
    end
  end
end
