class ScriptValidator < ActiveModel::EachValidator
  def validate_each(object, attribute, value)
    unless value.length > 0
      object.errors[attribute] << (options[:message] || "is not formatted properly") 
    end
  end
end